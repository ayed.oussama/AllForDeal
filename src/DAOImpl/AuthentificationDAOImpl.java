/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.AuthenticficationDAO;
import entities.Membre;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ayed
 */
public class AuthentificationDAOImpl implements AuthenticficationDAO<Membre>{
    private Connection connection;
    @Override
    public boolean SignUpValid(String log, String pass) {
      
        boolean res=false;
        String sql = "SELECT login,motDePasse FROM membre "; 
         PreparedStatement preparedStatement = null;
         ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            if (resultSet.next()) {
               
                
               if(log.equals(resultSet.getString("login")) && pass.equals(resultSet.getString("motDePasses"))  ){
                   res=true;
               }
                
            }


        } catch (SQLException ex) {
            Logger.getLogger(AuthentificationDAOImpl.class.getName()).log(Level.SEVERE, "find employee failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AuthentificationDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return res;
    
    
    }

    @Override
    public void SignIn(Membre u) {
       
            String sql = "INSERT INTO membre (login,motDePasse,nom,prenom,genre,adresse,type) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setString(1, u.getLogin());
                preparedStatement.setString(2, u.getMotDePasse());
                preparedStatement.setString(3, u.getNom());
                preparedStatement.setString(4, u.getPrenom());
                preparedStatement.setString(5, u.getGenre());
                preparedStatement.setString(6, u.getAdresse());
                preparedStatement.setString(7, u.getType());
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(AuthentificationDAOImpl.class.getName()).log(Level.SEVERE, "insert failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AuthentificationDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }    
    
    
}
