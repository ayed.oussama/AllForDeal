/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.crudDAO;
import entities.Produit;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ayed
 */
public class ProduitDAOimpl implements crudDAO<Produit>{
        private Connection connection;

    @Override
    public Produit find(int id) {
           String sql = "SELECT * FROM produit WHERE id=?"; 
        Produit found = null; 
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            if (resultSet.next()) {
               
                
                found = new Produit (resultSet.getFloat("prix"),resultSet.getString("couleur"), resultSet.getInt("nombrePoints"),resultSet.getFloat("tauxReduction"),resultSet.getDate("dateLancement"),resultSet.getString("etat"),resultSet.getString("texte"),resultSet.getByte("photo"),resultSet.getString("gouvernorat"),resultSet.getString("delegation"));
                
            }


        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "find produit failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return found;
    }

    @Override
    public void ajouter(Produit t) {
       if (find(t.getId()) == null) {
            String sql = "INSERT INTO produit (prix,couleur,nombrePoints,tauxReduction,dateLancement,etat,texte,photo,gouvernorat,delegation) VALUES (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setFloat(1, t.getPrix());
                preparedStatement.setString(2, t.getCouleur());
                preparedStatement.setInt(3, t.getNombrePoints());
                preparedStatement.setFloat(4, t.getTauxReduction());
                preparedStatement.setDate(5, (Date) t.getDateLancement());
                preparedStatement.setString(6, t.getEtat());
                preparedStatement.setString(7, t.getTexte());
                preparedStatement.setByte(8, t.getPhoto());
                preparedStatement.setString(9, t.getGouvernorat());
                preparedStatement.setString(10, t.getDelegation());
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "insert failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }    
    }

    @Override
    public void modifier(Produit t) {
        if (find(t.getId()) == null) {
            String sql = "update produit set prix=?,couleur=?,nombrePoints=?,tauxReduction=?,dateLancement=?,etat=?,texte=?,photo=?,gouvernorat=?,delegation=? where id=?";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setFloat(1, t.getPrix());
                preparedStatement.setString(2, t.getCouleur());
                preparedStatement.setInt(3, t.getNombrePoints());
                preparedStatement.setFloat(4, t.getTauxReduction());
                preparedStatement.setDate(5, (Date) t.getDateLancement());
                preparedStatement.setString(6, t.getEtat());
                preparedStatement.setString(7, t.getTexte());
                preparedStatement.setByte(8, t.getPhoto());
                preparedStatement.setString(9, t.getGouvernorat());
                preparedStatement.setString(10, t.getDelegation());
                preparedStatement.setInt(11, t.getId());
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "update failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }  
    }

    @Override
    public void supprimer(Produit t) {
      if (find(t.getId()) == null) {
            String sql = "delete from Produit where id=?";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setInt(1,t.getId());
                
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "delete failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }      
    }

    @Override
    public List<Produit> findAll() {
       String sql = "SELECT * FROM produit";
        Produit found ;
        List<Produit> listProduits = null; 
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            while (resultSet.next()) {
               
                
                found = new Produit (resultSet.getFloat("prix"),resultSet.getString("couleur"), resultSet.getInt("nombrePoints"),resultSet.getFloat("tauxReduction"),resultSet.getDate("dateLancement"),resultSet.getString("etat"),resultSet.getString("texte"),resultSet.getByte("photo"),resultSet.getString("gouvernorat"),resultSet.getString("delegation"));
                listProduits.add(found);
            }


        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "find produit failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ProduitDAOimpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return listProduits;
    }
    
}
