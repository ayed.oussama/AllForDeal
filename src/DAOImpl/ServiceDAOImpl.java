/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.crudDAO;
import entities.Service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ayed
 */
public class ServiceDAOImpl implements crudDAO<Service> {
    private Connection connection;
    @Override
    public Service find(int id) {
         String sql = "SELECT * FROM service WHERE id=?"; 
        Service found = null; 
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            if (resultSet.next()) {
               
                
                found = new Service (resultSet.getString("type"), resultSet.getString("description"));
                
            }


        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "find service failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return found;
    }

    @Override
    public void ajouter(Service t) {
        if (find(t.getId()) == null) {
            String sql = "INSERT INTO Service (type, description) VALUES (?,?)";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setString(1, t.getType());
                preparedStatement.setString(2, t.getDescription());
               
                
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "insert failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }    
    }

    @Override
    public void modifier(Service t) {
        if (find(t.getId()) == null) {
            String sql = "update service set type=?, description=? where id=?";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setString(1, t.getType());
                preparedStatement.setString(2, t.getDescription());
                preparedStatement.setInt(3,t.getId());
               
                
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "update failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }    
    }

    @Override
    public void supprimer(Service t) {
        if (find(t.getId()) == null) {
            String sql = "delete from service where id=?";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setInt(1, t.getId());
               
               
                
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "delete failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
        }    
    }

    @Override
    public List<Service> findAll() {
        String sql = "SELECT * FROM service";
        Service found ;
        List<Service> listServices = null; 
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            while (resultSet.next()) {
               
                
                found = new Service (resultSet.getString("type"), resultSet.getString("description"));
                listServices.add(found);
            }


        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "find service failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ServiceDAOImpl.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return listServices;
    }
    
}
