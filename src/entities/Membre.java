/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Dagdoug
 */
public class Membre {

    private int id;
    private String login;
    private String motDePasse;
    private String nom;
    private String prenom;
    private String genre;
    private String adresse;
    private String type;
    private String email;
    private Avis[] avis;
    private Panier panier;
    private Reclamation[] Reclamations;

    public Membre() {
    }

    public Membre(String login, String motDePasse, String nom, String prenom, String genre, String adresse, String type) {
        this.login = login;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.genre = genre;
        this.adresse = adresse;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Avis[] getAvis() {
        return avis;
    }

    public void setAvis(Avis[] avis) {
        this.avis = avis;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public Reclamation[] getReclamations() {
        return Reclamations;
    }

    public void setReclamations(Reclamation[] Reclamations) {
        this.Reclamations = Reclamations;
    }

    public void modifierProfill() {

    }

    public void repondreAUnAppelD_offre() {

    }

    public void acheterProduit() {

    }

    public void acheterService() {

    }

}
