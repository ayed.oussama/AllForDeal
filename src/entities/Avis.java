/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Dagdoug
 */
public class Avis {

    private int id;
    private String commentaire;
    private String evaluation;

    public Avis() {
    }

    public Avis(String commentaire, String evaluation) {
        this.commentaire = commentaire;
        this.evaluation = evaluation;
    }

    public int getId() {
        return id;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

}
