/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Dagdoug
 */
public class Offre {

    private int id;
    private String titreOffre;
    private Categorie categorie;
    private Avis[] avis;

    public Offre() {
    }

    public Offre(String titreOffre) {
        this.titreOffre = titreOffre;
    }

    public int getId() {
        return id;
    }

    public String getTitreOffre() {
        return titreOffre;
    }

    public void setTitreOffre(String titreOffre) {
        this.titreOffre = titreOffre;
    }

    public Avis[] getAvis() {
        return avis;
    }

    public void setAvis(Avis[] avis) {
        this.avis = avis;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

}
