/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Dagdoug
 */
public class Paiement {

    private int id;
    private float somme;
    private Panier panier;
    
    public Paiement() {
    }

    public Paiement(float somme, Panier panier) {
        this.somme = somme;
        this.panier = panier;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }


    public Paiement(float somme) {
        this.somme = somme;
    }

    public int getId() {
        return id;
    }

    public float getSomme() {
        return somme;
    }

    public void setSomme(float somme) {
        this.somme = somme;
    }

}
