/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;

/**
 *
 * @author Ayed
 */
public interface PanierDAO<Panier>{
    public abstract Panier find(int id);
    public abstract List<Panier> findAll(); 
    public abstract void modifier(Panier t);
    public abstract void supprimer(Panier t); 
    public abstract void viderPanier();
}
