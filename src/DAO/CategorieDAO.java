/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;

/**
 *
 * @author Ayed
 */
public interface CategorieDAO<Categorie>  {
    public abstract Categorie find(int id);
    public abstract List<Categorie> findAll(); 
    public abstract void ajouter(Categorie t);
    public abstract void supprimer(Categorie t);
}
