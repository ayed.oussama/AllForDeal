/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import Connection.Connection;
import java.util.List;

/**
 *
 * @author Ayed
 */
public interface crudDAO<T> {
    Connection connection = Connection.getInstance().getConnection();
    public abstract T find(int id);
    public abstract void ajouter(T t);
    public abstract void modifier(T t);
    public abstract void supprimer(T t);
    public abstract List<T> findAll();  
}
