/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connection;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ayed
 */
public class Connection {
    private String url ;
    private String login ;
    private String password ;
    private static Connection instance ;
    private Connection connection ;
    private Connection(){
        try {
            Properties properties = new Properties();
            
            properties.load(new FileInputStream(new File("configuration.properties")));
            url = properties.getProperty("url");
            login = properties.getProperty("login");
            password = properties.getProperty("password");
            connection = (Connection) DriverManager.getConnection(url , login , password );
            System.out.println("Connection etablie !!");
        } catch (IOException |SQLException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static Connection getInstance(){
        if (instance == null)
            instance = new Connection();
         return  instance ;
    }
    public Connection getConnection(){
        return connection ;
    }
}